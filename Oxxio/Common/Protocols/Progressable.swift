//
//  Progressable.swift
//  Oxxio
//
//  Created by Igor Lipovac on 5/6/16
//  Copyright (c) 2016 TamTam. All rights reserved.
//

import UIKit

protocol Progressable {
    func showLoading()
    func hideLoading()
}

extension Progressable where Self: UIViewController {
    
    func showLoading() {
    }
    
    func hideLoading() {
    }
    
}
