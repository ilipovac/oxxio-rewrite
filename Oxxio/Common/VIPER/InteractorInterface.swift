//
//  InteractorInterface.swift
//  Oxxio
//
//  Created by Igor Lipovac on 5/6/16
//  Copyright (c) 2016 TamTam. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Base interactor interface -
protocol InteractorInterface {
}

// MARK: - Base interactor interface default implementation -
extension InteractorInterface {
}

