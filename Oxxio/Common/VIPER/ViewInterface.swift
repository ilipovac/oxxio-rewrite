//
//  ViewInterface.swift
//  Oxxio
//
//  Created by Igor Lipovac on 5/6/16
//  Copyright (c) 2016 TamTam. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Base view interface -
protocol ViewInterface: class {
}

// MARK: - Base view interface default implementation -
extension ViewInterface {
}
