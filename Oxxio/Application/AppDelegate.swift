//
//  AppDelegate.swift
//  Oxxio
//
//  Created by Igor Lipovac on 5/6/16
//  Copyright (c) 2016 TamTam. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}
